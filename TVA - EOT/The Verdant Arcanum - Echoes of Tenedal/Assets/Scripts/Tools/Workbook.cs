using System.Collections.Generic;

namespace TheVerdantArcanum.Tools
{
    public class Workbook
    {
        public List<Sheet> Sheets { get; set; } = new List<Sheet>();

        public Sheet GetSheetByName(string name)
        {
            foreach (var sheet in Sheets)
            {
                if (sheet.Name == name) return sheet;
            }

            return null;
        }

        public void AddSheet(Sheet sheet)
        {
            Sheets.Add(sheet);
        }
    }
}