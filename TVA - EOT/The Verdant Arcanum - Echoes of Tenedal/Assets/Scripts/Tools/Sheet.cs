using System.Collections.Generic;

namespace TheVerdantArcanum.Tools
{
    public class Sheet
    {
        public string Name { get; set; }
        private List<List<string>> rows = new();

        public Sheet(string name)
        {
            Name = name;
        }

        public void AddRow(List<string> rowData)
        {
            rows.Add(rowData);
        }

        public List<string> GetRow(int rowIndex)
        {
            return rowIndex >= 0 && rowIndex < rows.Count ? rows[rowIndex] : new List<string>();
        }

        public string Cell(string column, int rowIndex)
        {
            var columnIndex = ColumnIndex(column);
            var row = GetRow(rowIndex);

            return columnIndex >= 0 && columnIndex < row.Count ? row[columnIndex] : null;
        }

        private int ColumnIndex(string column)
        {
            var index = 0;
            var multiplier = 1;

            for (var i = column.Length - 1; i >= 0; i--)
            {
                index += (column[i] - 'A' + 1) * multiplier - 1;
                multiplier *= 26;
            }

            return index;
        }
    }
}