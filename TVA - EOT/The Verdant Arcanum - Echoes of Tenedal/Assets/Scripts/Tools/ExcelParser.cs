using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;

namespace TheVerdantArcanum.Tools
{
    public class ExcelParser
    {
        public Workbook Parse(string filePath)
        {
            var workbook = new Workbook();
            var sheetNames = ParseSheetNames(filePath);
            var sharedStrings = ParseSharedStrings(filePath); // Load shared strings
            ParseSheets(filePath, sheetNames, sharedStrings, workbook);
            return workbook;
        }

        private Dictionary<string, string> ParseSheetNames(string filePath)
        {
            var sheetNames = new Dictionary<string, string>();

            using (ZipArchive archive = ZipFile.OpenRead(filePath))
            {
                ZipArchiveEntry workbookXml = archive.GetEntry("xl/workbook.xml");
                if (workbookXml != null)
                {
                    using var stream = workbookXml.Open();
                    using XmlReader reader = XmlReader.Create(stream);
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "sheet")
                        {
                            var name = reader.GetAttribute("name");
                            var id = reader.GetAttribute("sheetId"); // or use "r:id" depending on your file's structure
                            sheetNames.Add(id, name);
                        }
                    }
                }
            }
            return sheetNames;
        }

        private void ParseSheets(string filePath, Dictionary<string, string> sheetNames, List<string> sharedStrings, Workbook workbook)
        {
            using var archive = ZipFile.OpenRead(filePath);

            foreach (var sheetName in sheetNames)
            {
                var sheetPath = $"xl/worksheets/sheet{sheetName.Key}.xml";
                var entry = archive.GetEntry(sheetPath);
                if (entry != null)
                {
                    var sheet = new Sheet(sheetName.Value);
                    using (var stream = entry.Open())
                    using (var reader = XmlReader.Create(stream))
                    {
                        var currentRow = new List<string>();
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Element && reader.Name == "c")
                            {
                                var cellType = reader.GetAttribute("t");
                                reader.ReadToFollowing("v");
                                var value = reader.ReadElementContentAsString();

                                if (cellType == "s")
                                {
                                    var index = int.Parse(value);
                                    currentRow.Add(sharedStrings[index]);
                                }
                                else
                                {
                                    currentRow.Add(value);
                                }
                            }
                            else if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "row")
                            {
                                sheet.AddRow(new List<string>(currentRow));
                                currentRow.Clear();
                            }
                        }
                    }
                    workbook.AddSheet(sheet);
                }
            }
        }

        private List<string> ParseSharedStrings(string filePath)
        {
            List<string> sharedStrings = new();
            using (ZipArchive archive = ZipFile.OpenRead(filePath))
            {
                var sharedStringsEntry = archive.GetEntry("xl/sharedStrings.xml");
                if (sharedStringsEntry != null)
                {
                    using var stream = sharedStringsEntry.Open();
                    using XmlReader reader = XmlReader.Create(stream);
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "t")
                        {
                            sharedStrings.Add(reader.ReadElementContentAsString());
                        }
                    }
                }
            }
            return sharedStrings;
        }
    }
}