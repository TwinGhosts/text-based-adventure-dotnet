using TheVerdantArcanum.Commands;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Enums;
using TheVerdantArcanum.Services;
using UnityEngine;

namespace TheVerdantArcanum.Behaviours
{
    public class GameBehaviour : MonoBehaviour
    {
        [SerializeField] private InputService inputService;

        private void Start()
        {
            var gameDataService = new GameDataService();
            var gameData = gameDataService.LoadGameData();

            gameData.Rooms[0].DisplayInformation();

            inputService.Initialize(new Player(gameData), gameData);
        }
    }
}