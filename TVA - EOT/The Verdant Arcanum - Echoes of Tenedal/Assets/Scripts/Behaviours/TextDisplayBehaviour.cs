using UnityEngine;
using TMPro;
using TheVerdantArcanum.Enums;

namespace TheVerdantArcanum.Behaviours
{
    public class TextDisplayBehaviour : MonoBehaviour
    {
        [SerializeField] private TMP_Text textPrefab;
        [SerializeField] private Transform textParent;

        public static TextDisplayBehaviour Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void DisplayText(string text, DisplayLogType logType = DisplayLogType.General, bool addCaret = false)
        {
            var logText = addCaret ? "> " : string.Empty;
            logText += $"<color={GetColorString(logType)}>{text}</color>\n";

            var textInstance = Instantiate(textPrefab, textParent);
            textInstance.text = logText;
        }

        public string GetColorString(DisplayLogType logType)
        {
            switch (logType)
            {
                default:
                case DisplayLogType.General:
                    return "#FBF7F4";
                case DisplayLogType.Error:
                    return "#C62E65";
                case DisplayLogType.Warning:
                    return "#FF784F";
                case DisplayLogType.Command:
                    return "#99E1D9";
                case DisplayLogType.Info:
                    return "#EDFFD9";
            }
        }
    }
}