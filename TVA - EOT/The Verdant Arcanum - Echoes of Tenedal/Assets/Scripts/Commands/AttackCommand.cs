﻿using System.Collections.Generic;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Interfaces;
using UnityEngine;

namespace TheVerdantArcanum.Commands
{
    public class AttackCommand : ICommand
    {
        public List<string> CommandInputs => new List<string>
        {
            "attack",
            "a"
        };

        public void Execute(Player player, List<string> arguments, object target)
        {
            Debug.Log($"Attack Command: '{arguments}' | {target}");
        }
    }
}
