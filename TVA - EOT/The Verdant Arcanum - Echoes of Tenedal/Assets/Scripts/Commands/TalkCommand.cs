﻿using System.Collections.Generic;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Interfaces;
using UnityEngine;

namespace TheVerdantArcanum.Commands
{
    public class TalkCommand : ICommand
    {
        public List<string> CommandInputs => new List<string>
        {
            "talk",
            "t"
        };

        public void Execute(Player player, List<string> arguments, object target)
        {
            if (target is ITalkable)
            {
                (target as ITalkable)?.Talk();
            }
            else
            {
                Debug.Log($"'{target}' cannot be talked to!");
            }
        }
    }
}
