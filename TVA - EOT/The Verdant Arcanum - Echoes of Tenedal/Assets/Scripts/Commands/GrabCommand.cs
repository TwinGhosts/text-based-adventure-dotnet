﻿using System.Collections.Generic;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Interfaces;
using UnityEngine;

namespace TheVerdantArcanum.Commands
{
    public class GrabCommand : ICommand
    {
        public List<string> CommandInputs => new List<string>
        {
            "grab",
            "g"
        };

        public void Execute(Player player, List<string> arguments, object target)
        {
            Debug.Log($"Grab Command: '{arguments}' | {target}");
        }
    }
}
