﻿using System.Collections.Generic;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Interfaces;
using UnityEngine;

namespace TheVerdantArcanum.Commands
{
    public class InspectCommand : ICommand
    {
        public List<string> CommandInputs => new List<string>
        {
            "inspect",
            "i"
        };

        public void Execute(Player player, List<string> arguments, object target)
        {
            if (target is IInspectable)
            {
                (target as IInspectable)?.Inspect();
            }
            else
            {
                Debug.Log($"'{target}' cannot be inspected!");
            }
        }
    }
}
