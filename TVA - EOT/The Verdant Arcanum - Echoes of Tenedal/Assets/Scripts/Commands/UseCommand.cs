﻿using System.Collections.Generic;
using System.Linq;
using TheVerdantArcanum.Behaviours;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Enums;
using TheVerdantArcanum.Interfaces;

namespace TheVerdantArcanum.Commands
{
    public class UseCommand : ICommand
    {
        public List<string> CommandInputs => new()
        {
            "use",
            "u"
        };

        public void Execute(Player player, List<string> arguments, object target)
        {
            if (arguments.Count == 0)
            {
                TextDisplayBehaviour.Instance.DisplayText("You must specify a target to use or the object and a target to use it on!", DisplayLogType.Error);
                return;
            }

            var itemName = arguments[0];
            var item = player.Inventory.Items.FirstOrDefault(i => i.Name.ToLowerInvariant() == itemName);
            if (item == null)
            {
                TextDisplayBehaviour.Instance.DisplayText($"You don't have '{itemName}' in your inventory", DisplayLogType.Error);
            }

            if (arguments.Count == 1 && item != null)
            {
                TextDisplayBehaviour.Instance.DisplayText($"You use '{itemName}'", DisplayLogType.General);
            }
            else if (arguments.Count == 2 && item != null)
            {
                TextDisplayBehaviour.Instance.DisplayText($"You use '{itemName}' on '{arguments[1]}'", DisplayLogType.General);
            }
        }
    }
}
