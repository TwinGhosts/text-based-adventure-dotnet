﻿using System.Collections.Generic;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Interfaces;
using UnityEngine;

namespace TheVerdantArcanum.Commands
{
    public class LookCommand : ICommand
    {
        public List<string> CommandInputs => new List<string>
        {
            "look",
            "l"
        };

        public void Execute(Player player, List<string> arguments, object target)
        {
            Debug.Log($"Look Command: '{arguments}' | {target}");
        }
    }
}
