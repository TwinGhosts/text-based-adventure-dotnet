﻿using System.Collections.Generic;
using TheVerdantArcanum.Behaviours;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Enums;
using TheVerdantArcanum.Interfaces;

namespace TheVerdantArcanum.Commands
{
    public class MoveCommand : ICommand
    {
        public List<string> CommandInputs => new()
        {
            "move",
            "m"
        };

        public void Execute(Player player, List<string> arguments, object target)
        {
            var room = player.CurrentRoom;
            var exits = room.Exits;

            if (arguments.Count > 0)
            {
                var direction = string.Join(' ', arguments);
                if (exits.ContainsKey(direction))
                {
                    var newRoom = exits[direction];
                    player.MoveToRoom(newRoom);

                    newRoom.DisplayInformation();
                }
                else
                {
                    TextDisplayBehaviour.Instance.DisplayText($"There is no exit in towards {direction}", DisplayLogType.Error);
                }
            }
        }
    }
}
