﻿namespace TheVerdantArcanum.Interfaces
{
    public interface ILookable
    {
        string LookText { get; set; }

        void Look();
    }
}
