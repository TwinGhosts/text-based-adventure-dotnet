﻿namespace TheVerdantArcanum.Interfaces
{
    public interface IInspectable
    {
        string InspectionText { get; set; }

        void Inspect();
    }
}
