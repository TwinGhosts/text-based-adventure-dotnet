﻿namespace TheVerdantArcanum.Interfaces
{
    public interface ITalkable
    {
        string DialogueID { get; set; }

        void Talk();
    }
}
