﻿using System.Collections.Generic;
using TheVerdantArcanum.Entities;

namespace TheVerdantArcanum.Interfaces
{
    public interface ICommand
    {
        public List<string> CommandInputs { get; }

        void Execute(Player player, List<string> arguments, object target);
    }
}
