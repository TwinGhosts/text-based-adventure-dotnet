﻿namespace TheVerdantArcanum.Interfaces
{
    public interface IGrabbable
    {
        void Grab();
    }
}
