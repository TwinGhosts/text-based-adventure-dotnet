﻿namespace TheVerdantArcanum.Interfaces
{
    public interface IAttackable
    {
        void Attack();
    }
}
