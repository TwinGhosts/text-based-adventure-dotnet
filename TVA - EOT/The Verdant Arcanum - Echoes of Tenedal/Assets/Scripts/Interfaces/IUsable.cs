﻿namespace TheVerdantArcanum.Interfaces
{
    public interface IUsable
    {
        void Use();
    }
}
