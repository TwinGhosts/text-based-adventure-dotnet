using System.Collections.Generic;

namespace TheVerdantArcanum.Entities
{
    public class GameData
    {
        public List<Story> Stories { get; set; } = new();
        public List<Room> Rooms { get; set; } = new();
        public List<Item> Items { get; set; } = new();
        public List<Character> Characters { get; set; } = new();
        public List<Dialogue> Dialogues { get; set; } = new();
        public List<Misc> Misc { get; set; } = new();
    }
}
