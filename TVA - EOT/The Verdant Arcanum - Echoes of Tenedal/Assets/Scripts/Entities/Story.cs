namespace TheVerdantArcanum.Entities
{
    public class Story
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Content { get; set; } = "";
    }
}
