﻿namespace TheVerdantArcanum.Entities
{
    public class Stats
    {
        public int Level { get; set; }
        public int Experience { get; set; }
        public int TalentPoints { get; set; }
        public int Health { get; set; }
        public int MaxHealth { get; set; }
        public int Mana { get; set; }
        public int MaxMana { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Strength { get; set; }
        public int Agility { get; set; }
        public int Intelligence { get; set; }
        public int Perception { get; set; }
    }
}
