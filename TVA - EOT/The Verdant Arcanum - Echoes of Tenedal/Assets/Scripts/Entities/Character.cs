namespace TheVerdantArcanum.Entities
{
    public class Character
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
    }
}
