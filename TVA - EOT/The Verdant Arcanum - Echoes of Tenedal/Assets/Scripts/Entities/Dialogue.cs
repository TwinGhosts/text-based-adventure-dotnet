namespace TheVerdantArcanum.Entities
{
    public class Dialogue
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Content { get; set; } = "";
    }
}
