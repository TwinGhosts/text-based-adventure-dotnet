namespace TheVerdantArcanum.Entities
{
    public class StackableItem : Item
    {
        public int Stacks { get; set; } = 1;
        public int MaxStacks { get; set; } = 1;
    }
}