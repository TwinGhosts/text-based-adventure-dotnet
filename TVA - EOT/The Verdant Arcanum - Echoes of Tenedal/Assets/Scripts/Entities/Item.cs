namespace TheVerdantArcanum.Entities
{
    public class Item
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public int Weight { get; set; } = 1;
        public int Size { get; set; } = 1;

        public virtual void Use()
        {
            // TODO: Use logic here
        }
    }
}
