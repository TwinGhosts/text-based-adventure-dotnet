using System.Collections.Generic;
using TheVerdantArcanum.Behaviours;
using TheVerdantArcanum.Enums;

namespace TheVerdantArcanum.Entities
{
    public class Room
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<Item> Items { get; set; } = new();
        public List<Character> Characters { get; set; } = new();
        public Dictionary<string, Room> Exits { get; set; } = new();
        public List<string> ExitIds { get; set; } = new();

        public void DisplayInformation()
        {
            var exits = string.Join(", ", Exits.Keys);

            TextDisplayBehaviour.Instance.DisplayText($"Exits: {exits}\n", DisplayLogType.Warning);
            TextDisplayBehaviour.Instance.DisplayText($"{Description}", DisplayLogType.General);
            TextDisplayBehaviour.Instance.DisplayText($"{Name}", DisplayLogType.Info);
        }

        public T FindByName<T>(string name) where T : class
        {
            return Items.Find(item => item.Name.ToLower() == name.ToLower()) as T
                ?? Characters.Find(character => character.Name.ToLower() == name.ToLower()) as T;
        }

        public Room GetExit(string exitName)
        {
            return Exits.GetValueOrDefault(exitName.ToLower());
        }
    }
}
