﻿using System;

namespace TheVerdantArcanum.Entities
{
    public class Descriptives
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; } = "Name";
        public string Description { get; set; } = "Description";
    }
}
