﻿using System;
using TheVerdantArcanum.Services;

namespace TheVerdantArcanum.Entities
{
    public class Player
    {
        public Descriptives Descriptives { get; private set; } = new();
        public Stats Stats { get; private set; } = new();
        public Room CurrentRoom { get; private set; }

        public Action<int> OnExperienceGained = (int experience) => { };
        public Action<int> OnLevelUp = (int level) => { };
        public Action<Room> OnRoomChanged = (Room room) => { };

        public InventoryService Inventory { get; private set; }

        public Player(GameData dataModel)
        {
            if (dataModel != null)
            {
                CurrentRoom = dataModel.Rooms[0];
            }

            Inventory = new InventoryService();
        }

        public void SetStats(int health, int maxHealth, int attack, int defense, int strength, int agility, int intelligence, int perception)
        {
            Stats ??= new();

            Stats.Attack = attack;
            Stats.Defense = defense;
            Stats.Health = health;
            Stats.MaxHealth = maxHealth;
            Stats.Strength = strength;
            Stats.Agility = agility;
            Stats.Intelligence = intelligence;
            Stats.Perception = perception;
        }

        public void MoveToRoom(Room room)
        {
            CurrentRoom = room;

            if (room != CurrentRoom)
            {
                CurrentRoom.DisplayInformation();
                OnRoomChanged.Invoke(room);
            }
        }

        public void GainExperience(int experience)
        {
            Stats.Experience += experience;
            if (Stats.Experience >= 100)
            {
                Stats.Level++;
                Stats.Experience = 0;

                OnLevelUp.Invoke(Stats.Level);
            }

            OnExperienceGained.Invoke(experience);
        }
    }
}
