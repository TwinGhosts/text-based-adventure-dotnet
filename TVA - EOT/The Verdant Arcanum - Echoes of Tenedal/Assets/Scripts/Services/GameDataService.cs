using System.Collections.Generic;
using System.IO;
using System.Linq;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Tools;
using UnityEngine;

namespace TheVerdantArcanum.Services
{
    public class GameDataService
    {
        private const string STORY_SHEET_NAME = "Story";
        private const string ROOMS_SHEET_NAME = "Rooms";
        private const string ITEMS_SHEET_NAME = "Items";
        private const string CHARACTERS_SHEET_NAME = "Characters";
        private const string DIALOGUE_SHEET_NAME = "Dialogue";
        private const string MISC_SHEET_NAME = "Misc";

        public GameData LoadGameData()
        {
            var filePath = Path.Combine(Application.streamingAssetsPath, "Content-EN.xlsx");
            var gameData = new GameData();
            var excelParser = new ExcelParser();


            var workbook = excelParser.Parse(filePath);

            gameData.Stories = LoadStory(workbook);
            gameData.Rooms = LoadRooms(workbook);
            gameData.Items = LoadItems(workbook);
            gameData.Characters = LoadCharacters(workbook);
            gameData.Dialogues = LoadDialogue(workbook);
            gameData.Misc = LoadMisc(workbook);

            gameData = LinkRooms(gameData);

            return gameData;
        }

        private GameData LinkRooms(GameData gameData)
        {
            var rooms = gameData.Rooms;
            foreach (var room in rooms)
            {
                var exits = new Dictionary<string, Room>();
                foreach (var exitId in room.ExitIds)
                {
                    var exit = rooms.Find(r => r.Id == exitId);
                    if (exit != null)
                    {
                        exits.Add(exit.Name.ToLowerInvariant(), exit);
                    }
                }

                room.Exits = exits;
            }

            gameData.Rooms = rooms;
            return gameData;
        }

        private List<Story> LoadStory(Workbook workbook)
        {
            var stories = new List<Story>();
            var storySheet = workbook.GetSheetByName(STORY_SHEET_NAME);

            var rowIndex = 1;
            foreach (var row in storySheet.GetRow(rowIndex))
            {
                var id = storySheet.Cell("A", rowIndex);
                if (id == null)
                {
                    continue;
                }

                var name = storySheet.Cell("B", rowIndex);
                var content = storySheet.Cell("C", rowIndex);

                stories.Add(new Story
                {
                    Id = id,
                    Name = name,
                    Content = content
                });

                rowIndex++;
            }

            return stories;
        }

        private List<Room> LoadRooms(Workbook workbook)
        {
            var rooms = new List<Room>();
            var roomSheet = workbook.GetSheetByName(ROOMS_SHEET_NAME);

            var rowIndex = 1;
            foreach (var row in roomSheet.GetRow(rowIndex))
            {
                var id = roomSheet.Cell("A", rowIndex);
                if (id == null)
                {
                    continue;
                }

                var name = roomSheet.Cell("B", rowIndex);
                var content = roomSheet.Cell("C", rowIndex);
                var exitsString = roomSheet.Cell("D", rowIndex);
                var exitIds = exitsString?.Split(',').ToList() ?? new List<string>();

                rooms.Add(new Room
                {
                    Id = id,
                    Name = name,
                    Description = content,
                    ExitIds = exitIds,
                });

                rowIndex++;
            }

            return rooms;
        }

        private List<Item> LoadItems(Workbook workbook)
        {
            var items = new List<Item>();
            var itemSheet = workbook.GetSheetByName(ITEMS_SHEET_NAME);

            var rowIndex = 1;
            foreach (var row in itemSheet.GetRow(rowIndex))
            {
                var id = itemSheet.Cell("A", rowIndex);
                if (id == null)
                {
                    continue;
                }

                var name = itemSheet.Cell("B", rowIndex);
                var content = itemSheet.Cell("C", rowIndex);

                items.Add(new Item
                {
                    Id = id,
                    Name = name,
                    Description = content
                });

                rowIndex++;
            }

            return items;
        }

        private List<Character> LoadCharacters(Workbook workbook)
        {
            var characters = new List<Character>();
            var characterSheet = workbook.GetSheetByName(CHARACTERS_SHEET_NAME);

            var rowIndex = 1;
            foreach (var row in characterSheet.GetRow(rowIndex))
            {
                var id = characterSheet.Cell("A", rowIndex);
                if (id == null)
                {
                    continue;
                }

                var name = characterSheet.Cell("B", rowIndex);
                var content = characterSheet.Cell("C", rowIndex);

                characters.Add(new Character
                {
                    Id = id,
                    Name = name,
                    Description = content
                });

                rowIndex++;
            }

            return characters;
        }

        private List<Dialogue> LoadDialogue(Workbook workbook)
        {
            var dialogues = new List<Dialogue>();
            var dialogueSheet = workbook.GetSheetByName(DIALOGUE_SHEET_NAME);

            var rowIndex = 1;
            foreach (var row in dialogueSheet.GetRow(rowIndex))
            {
                var id = dialogueSheet.Cell("A", rowIndex);
                if (id == null)
                {
                    continue;
                }

                var name = dialogueSheet.Cell("B", rowIndex);
                var content = dialogueSheet.Cell("C", rowIndex);

                dialogues.Add(new Dialogue
                {
                    Id = id,
                    Name = name,
                    Content = content
                });

                rowIndex++;
            }

            return dialogues;
        }

        private List<Misc> LoadMisc(Workbook workbook)
        {
            var misc = new List<Misc>();
            var miscSheet = workbook.GetSheetByName(MISC_SHEET_NAME);

            if (miscSheet == null)
                return misc;

            var rowIndex = 1;
            foreach (var row in miscSheet.GetRow(rowIndex))
            {
                if (row == null)
                    continue;

                var id = miscSheet.Cell("A", rowIndex);
                if (id == null)
                {
                    continue;
                }

                var name = miscSheet.Cell("B", rowIndex);
                var content = miscSheet.Cell("C", rowIndex);

                misc.Add(new Misc
                {
                    Id = id,
                    Name = name,
                    Content = content
                });

                rowIndex++;
            }

            return misc;
        }
    }
}
