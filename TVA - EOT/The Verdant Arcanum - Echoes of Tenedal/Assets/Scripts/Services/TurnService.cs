using System;

namespace TheVerdantArcanum.Services
{
    public class TurnService
    {
        public static TurnService Instance { get; private set; }

        public int CurrentTurn { get; private set; } = 0;

        public Action<int> OnTurnChange = (turn) => { };

        public void NextTurn()
        {
            CurrentTurn++;
            OnTurnChange?.Invoke(CurrentTurn);
        }
    }
}
