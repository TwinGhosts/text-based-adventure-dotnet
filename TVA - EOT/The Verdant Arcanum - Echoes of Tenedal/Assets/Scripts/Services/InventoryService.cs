using System.Collections.Generic;
using System.Linq;
using TheVerdantArcanum.Entities;

namespace TheVerdantArcanum.Services
{
    public class InventoryService
    {
        public List<Item> Items { get; private set; } = new();

        public int WeightLimit { get; private set; } = 10;
        public int SizeLimit { get; private set; } = 50;

        public void ChangeWeightLimit(int newLimit)
        {
            WeightLimit = newLimit;
        }

        public void ChangeSizeLimit(int newSize)
        {
            SizeLimit = newSize;
        }

        public bool AddItem(Item item)
        {
            if (item.Size + GetTotalSize() > SizeLimit || item.Weight + GetTotalWeight() > WeightLimit)
            {
                return false;
            }

            if (item is StackableItem)
            {
                item = item as StackableItem;
                foreach (StackableItem existingItem in Items.Where(item => item is StackableItem) as List<StackableItem>)
                {
                    if (existingItem.Id == item.Id && existingItem.Stacks < existingItem.MaxStacks)
                    {
                        existingItem.Stacks++;
                        return true;
                    }
                }
            }

            Items.Add(item);
            return true;
        }

        public bool RemoveItem(Item item, int amount = 1)
        {
            if (Items.Contains(item))
            {
                var stackableItem = item as StackableItem;
                if (stackableItem != null && stackableItem.Stacks > amount)
                {
                    stackableItem.Stacks -= amount;
                }
                else if (stackableItem != null && stackableItem.Stacks == amount)
                {
                    Items.Remove(item);
                }
                else
                {
                    var remainingAmount = amount;
                    foreach (StackableItem existingItem in Items.Where(item => item is StackableItem) as List<StackableItem>)
                    {
                        if (existingItem.Id == item.Id && existingItem.Stacks > 0)
                        {
                            if (existingItem.Stacks >= remainingAmount)
                            {
                                existingItem.Stacks -= remainingAmount;
                                break;
                            }
                            else
                            {
                                remainingAmount -= existingItem.Stacks;
                                existingItem.Stacks = 0;
                            }
                        }
                    }

                    if (remainingAmount > 0)
                    {
                        Items.Remove(item);
                    }
                }

                return true;
            }

            return false;
        }

        private int GetTotalSize()
        {
            int totalSize = 0;
            foreach (Item item in Items)
            {
                totalSize += item.Size;
            }
            return totalSize;
        }

        private int GetTotalWeight()
        {
            int totalWeight = 0;
            foreach (Item item in Items)
            {
                totalWeight += item.Weight;
            }
            return totalWeight;
        }
    }
}
