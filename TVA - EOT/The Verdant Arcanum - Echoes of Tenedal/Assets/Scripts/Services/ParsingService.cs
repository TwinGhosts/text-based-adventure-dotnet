using System.Collections.Generic;
using System.Linq;
using TheVerdantArcanum.Behaviours;
using TheVerdantArcanum.Commands;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Enums;
using TheVerdantArcanum.Interfaces;

namespace TheVerdantArcanum.Services
{
    public class ParsingService
    {
        private readonly AttackCommand attackCommand = new();
        private readonly GrabCommand grabCommand = new();
        private readonly InspectCommand inspectCommand = new();
        private readonly LookCommand lookCommand = new();
        private readonly MoveCommand moveCommand = new();
        private readonly UseCommand useCommand = new();
        private readonly TalkCommand talkCommand = new();

        private readonly List<ICommand> commands = new();

        private GameData gameData;

        public ParsingService(GameData gameData)
        {
            this.gameData = gameData;

            commands.Add(attackCommand);
            commands.Add(grabCommand);
            commands.Add(inspectCommand);
            commands.Add(lookCommand);
            commands.Add(moveCommand);
            commands.Add(useCommand);
            commands.Add(talkCommand);
        }

        public void Parse(string input, Player player)
        {
            var inputParts = input.Split(' ');
            var commandKey = inputParts.First();
            var arguments = inputParts.Skip(1).ToList();

            var command = commands.Find(c => c.CommandInputs.Contains(commandKey));
            if (command != null)
            {
                command.Execute(player, arguments, ParseTarget(command, player, arguments));
                return;
            }

            TextDisplayBehaviour.Instance.DisplayText("Unknown command!", DisplayLogType.Error);
        }

        private object ParseTarget(ICommand command, Player player, List<string> arguments)
        {
            var target = string.Join(" ", arguments);
            var currentRoom = player.CurrentRoom;

            return command switch
            {
                AttackCommand _ => currentRoom.FindByName<IAttackable>(target),
                GrabCommand _ => currentRoom.FindByName<IGrabbable>(target),
                InspectCommand _ => currentRoom.FindByName<IInspectable>(target),
                LookCommand _ => currentRoom.FindByName<ILookable>(target),
                TalkCommand _ => currentRoom.FindByName<ITalkable>(target),
                UseCommand _ => currentRoom.FindByName<IUsable>(target),
                MoveCommand _ => currentRoom.GetExit(target),
                _ => null,
            };
        }
    }
}
