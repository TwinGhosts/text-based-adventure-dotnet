using TheVerdantArcanum.Behaviours;
using TheVerdantArcanum.Entities;
using TheVerdantArcanum.Enums;
using TMPro;
using UnityEngine;

namespace TheVerdantArcanum.Services
{
    public class InputService : MonoBehaviour
    {
        [SerializeField] private TMP_InputField inputField;
        [SerializeField] private TextDisplayBehaviour textDisplayer;

        private ParsingService inputParser;
        private GameData gameData;
        private Player player;

        private void Awake()
        {
            inputField.onSubmit.AddListener(SubmitInput);
        }

        private void Update()
        {
            if (inputField != null && !inputField.isFocused)
            {
                inputField.Select();
                inputField.ActivateInputField();
            }
        }

        public void Initialize(Player player, GameData gameData)
        {
            this.player = player;
            this.gameData = gameData;
            inputParser = new ParsingService(gameData);
        }

        private void SubmitInput(string input)
        {
            input = input.ToLowerInvariant();

            textDisplayer.DisplayText($"{input}\n", DisplayLogType.Command, true);

            inputParser.Parse(input, player);
            inputField.text = string.Empty;
        }
    }
}
