namespace TheVerdantArcanum.Enums
{
    public enum DisplayLogType
    {
        General,
        Command,
        Warning,
        Error,
        Info,
    }
}